' Run this with the full path to a batch file that needs shell elevation.
' The batch file should check to see if it needs elevation first so that
' it doesn't call this infinitely.
'
' Typical batch file prologue (the %~f0 expands to the full path of the 
' currently executing batch file):
'
'   set __COMPAT_LAYER=RunAsInvoker
'   NET FILE >NUL 2>&1
'   if errorlevel 1 (
'       cscript elevateme.vbs //NoLogo "%~f0"
'       exit /B
'   )
'
' The __COMPAT_LAYER stuff is vaguely documented online but it's not
' clear to me what it does. 'NET FILE' attempts to execute a command
' which requires admin privileges. If it fails, invoke this script with
' the batch file as its name.
'
' The 'ELEV' is added to the end of the command's arguments so that can
' also be checked to see if (attempted) elevation has taken place, 
' should such a need present itself.
'
' This does not handle arguments with quotation marks nor spaces in them.

Set UAC = CreateObject("Shell.Application")

Set args = WScript.Arguments

Dim parameters
parameters = ""

If args.Count > 1 Then
    parameters = args.Item(1)

    For i = 2 To args.Count - 1
        parameters = parameters & " " & args.Item(i)
    Next
End If

UAC.ShellExecute args.Item(0), parameters & " ELEV", "", "runas", 1

' See also http://stackoverflow.com/questions/19672352/how-to-run-python-script-with-elevated-privilage-on-windows

