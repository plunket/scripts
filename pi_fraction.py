import itertools


def pi_frac(range_iter):
    """Compute fractions to estimate math.pi and print each next better one."""
    from math import pi
    closeness = 1
    for p in range_iter:
        denominator = p
        numerator = round(pi * p)
        val = numerator / denominator
        this_close = abs((val / pi) - 1.0)
        if this_close < closeness:
            closeness = this_close
            print(f"{numerator}/{denominator} = {val} ({(val/pi)*100.0}%) [{val-pi:+e}]")
            if closeness == 0:
                print("Stopping at limit of numeric precision; reached equivalence.")
                break


if __name__ == "__main__":
    pi_frac(itertools.count(1))
