#!/usr/bin/env python3

import glob
import json
import sys

try:
    import yaml
except ModuleNotFoundError:
    print("May need to pip install PyYaml.")
    sys.exit(1)


def Convert(inFile):
    y = yaml.safe_load(inFile.read())
    j = json.dumps(y, indent=4)
    return j


if __name__ == '__main__':
    args = sys.argv[1:]
    if not args:
        args = ["-"]
    for arg in args:
        if arg == '-':
            j = Convert(sys.stdin)
            sys.stdout.write(j)
        else:
            for filename in glob.iglob(arg):
                newFilename = f"{filename}.json"
                print(f"{filename} -> {newFilename}")
                with open(filename, 'rt') as inFile:
                    j = Convert(inFile)
                with open(newFilename, 'wt') as outFile:
                    outFile.write(j)
