@echo off
:: Execute with
:: C:\Windows\System32\cmd.exe /k "path\to\colortool-cmd.bat" "path\to\schemes-parent"

:: ColorTool can be downloaded from https://github.com/microsoft/terminal/releases/tag/1904.29002
:: It reads iTerm2 color schemes, many of which can be found in
::      https://github.com/mbadolato/iTerm2-Color-Schemes
:: They're in the 'schemes' directory.

setlocal EnableDelayedExpansion

cd /d "%~1"

for /f "tokens=*" %%s in ('powershell -Command "$(Get-ChildItem schemes\*.itermcolors | Get-Random).BaseName"') do set CHOICE=%%s
colortool --quiet "schemes\%CHOICE%.itermcolors"
title %CHOICE%
::echo %CHOICE%
