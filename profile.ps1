# Source this with ". <path>\scripts\profile.ps1" and it will create the default profile/"autoexec"
# file if it doesn't already exist. Just source it in a terminal and the profile will be created.
# The specific file is aliased as `$Profile` in Powershell.

# Color codes: https://i.sstatic.net/9UVnC.png

function prompt {
	$time = Get-Date -Format "hh:mm"
	$path = $ExecutionContext.SessionState.Path.CurrentLocation
	$nest = '>' * ($NestedPromptLevel+1)
	$esc = $([char]27)
	"`n$esc[33m[$time]$esc[0m $path`n$nest "
}

$default_profile = "$($Env:USERPROFILE)/Documents/WindowsPowerShell/profile.ps1"
if (-not (Test-Path -Path $default_profile)) {
	New-Item -Path $default_profile -ItemType "file" -Force -Value ". $PSScriptRoot\profile.ps1`n"
}
Remove-Variable default_profile
