from __future__ import print_function

"""\
Backup and restore folders.

Make a shortcut in %APPDATA%\Microsoft\Windows\SendTo to launch this script without parameters. Then
the thing(s) sent-to will be passed as the first parameter.
"""

#TEST_MODE = True

import os
import shutil
import subprocess
import sys

#EXCLUDED_EXTENSIONS = ['*.'+x for x in ('obj', 'lib', 'ilk', 'ncb', 'tlog')]

# for Py2:
try: input = raw_input
except NameError: pass

def DoExit(rv):
    input('\nPress ENTER to exit.')
    sys.exit(rv)

def InTestMode():
    try:
        return TEST_MODE
    except NameError:
        return False

def GetSourceAndTarget():
    numArgs = len(sys.argv)
    if (numArgs <= 1) or (numArgs > 3):
        print("Must select one or two folders for the operation.")
        DoExit(99)

    source = sys.argv[1]

    if numArgs == 2:
        root, ext = os.path.splitext(source)

        ext = ext.lower()
        if (ext == '.bak') or (ext.startswith('.bak') and ext[4:].lstrip().startswith('#')):
            target = root
        else:
            target = source + '.bak'
    elif numArgs == 3:
        target = sys.argv[2]

    return source, target

def MirrorDir(source, target):
    if os.path.exists(target) and not os.path.isdir(target):
        print("Selected target must be a directory.")
        DoExit(101)

    command = [
            'robocopy.exe', # r'c:\windows\system32\robocopy.exe',
            source,
            target,
            '/MIR', # Mirror source to target
            '/MT:8', # Multithread copies (8 is default if missing)
            '/NC', # No class (don't display "New File", "New Dir")
            '/NP', # No progress
            #'/NFL', # No file list
            '/NDL', # No directory list
            '/Z', # restartable mode
            '/XJ', # exclude junction points
        ]

    try:
        if EXCLUDED_EXTENSIONS:
            command.append('/XF')
            command.extend(['*.'+x for x in EXCLUDED_EXTENSIONS])
    except NameError:
        pass

    if InTestMode():
        #print('ARGS:')
        #print('\n'.join(sys.argv))
        #print('\nCOMMAND:')
        #print('\n'.join(command))
        command.append('/L') # List only, don't do.
    else:
        if os.path.exists(target):
            s = os.path.split(source)[1]
            t = os.path.split(target)[1]
            input("We're about to overwrite %s with %s. Press ENTER to continue." % (t, s))

    return subprocess.call(command)

try:
    source, target = GetSourceAndTarget()
    s = os.path.split(source)[1]
    t = os.path.split(target)[1]

    if os.path.exists(target):
        input("We're about to overwrite %s with %s. Press ENTER to continue." % (t, s))

    if os.path.isdir(source):
        rv = MirrorDir(source, target)
    else:
        if os.path.exists(target) and os.path.isdir(target):
            print("Selected target must be a file that we can delete.")
            DoExit(101)

        print("Copying %s to %s." % (s, t))
        #os.remove(target)
        shutil.copyfile(source, target)
        rv = 0

    DoExit(rv)

except SystemExit:
    pass

except KeyboardInterrupt:
    pass

except:
    import traceback
    traceback.print_exc()
    input("Press <ENTER> to continue.")

