"""Suspend Windows

Alternatively, this could be accomplished with
    python -c "import ctypes; ctypes.windll.PowrProf.SetSuspendState(0, 1, 0)"

Unfortunately, an oft-suggested solution of
    rundll32 PowrProf,SetSuspendState 0 1 0
fails to work because rundll32 implicitly passes the Window handle (for the desktop?) as the first parameter to the
function, which in this case is the flag to enter hibernation mode.
"""

import ctypes
from ctypes.wintypes import BOOLEAN

PowrProf = ctypes.windll.PowrProf

SetSuspendState = PowrProf.SetSuspendState
SetSuspendState.arguments = [ BOOLEAN, BOOLEAN, BOOLEAN ]
SetSuspendState.restype = BOOLEAN

SetSuspendState(0, 1, 0)
