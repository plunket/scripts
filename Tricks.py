#!/usr/bin/env python3

import sys
import copy

def dp(a):
    print (a)
    pass

def maxAppearances(seq):
    seq = list(copy.copy(seq)) # create a local copy of the sequence
    #dp(seq)
    maxVals = [] # we'll be returning a sorted list of values that have the maximum count
    maxCount = 0
    currentBase = 0 # this is generally where we're indexing in our list

    while currentBase < len(seq):
        v = seq[currentBase]
        #dp(f"{currentBase}: {v}")

        # set the current slot to -1 if it's not already accumulating values; this represents zero:
        # the number of occurrences is encoded as -(numOccurrences) - 1
        if v >= 0:
            seq[currentBase] = -1

        # now we need to chain through all of the slots as we overwrite them with initial counts
        # (but we conveniently skip any slots that have been accumulating)
        while v >= 0:
            v2 = seq[v]
            #dp(f"- {v}: {v2}")
            if v2 > 0:
                # when we're overwriting an unreached slot, set it to -2 which really means 1...
                c = seq[v] = -2
            else:
                # if we've already been accumulating in this slot just note another visit
                seq[v] -= 1
                c = seq[v]

            thisCount = -1 - c
            if thisCount > maxCount:
                maxCount = thisCount
                maxVals = [v]
            elif thisCount == maxCount:
                maxVals.append(v)

            # now follow the chain and go to the slot that we need to update if we've overwritten a
            # slot with a valid value in it already.
            v = v2

        # go back and start iterating from the head of the list again
        currentBase += 1

    #dp(seq)
    maxVals.sort()
    return (maxCount, maxVals)

def testMaxAppearances():
    testIndex = 0

    def Test(input, expected):
        nonlocal testIndex
        testIndex += 1
        count, value = maxAppearances(input)
        if value != expected[1]:
            print(f"Value that appeared most expected to be {expected[1]} but was {value} (test {testIndex}).")
        if count != expected[0]:
            print(f"Expected count to be {expected[0]} but it was {count} (test {testIndex}).")

    Test((), (0, []))
    Test((0, ), (1, [0]))
    Test((1, 1), (2, [1]))
    Test((1, 2, 5, 3, 1, 4), (2, [1]))
    Test((0, 1, 5, 3, 1, 4), (2, [1]))
    Test((0, 1, 2, 2, 2, 4), (3, [2]))
    Test((3, 3, 2, 2, 2, 3, 3), (4, [3]))
    Test((3, 3, 2, 2, 2, 3, 3, 2), (4, [2,3]))

testMaxAppearances()


#sys.exit(0)

def is_iterable(v):
    try:
        iter(v)
        return True
    except TypeError:
        return False

def flatten(seq):
    for el in seq:
        if is_iterable(el) and not isinstance(el, str):
            for sub in flatten(el):
                yield sub
        else:
            yield el

def testFlatten():
    def Compare(actual, expected):
        if actual != expected:
            print('Got', actual, 'but wanted', expected)

    def Print(x, _):
        t = type(x)
        print(t(flatten(x)))

    def Test(input, expected):
        t = type(input)
        Compare(t(flatten(input)), expected)

    #T = Print
    T = Test

    T([[1, 2, [3, 4], 5], 6, [7, 8, [9]]], [1, 2, 3, 4, 5, 6, 7, 8, 9])
    T(['abc', 'def', ['ghi', ['jkl'], 'mno'], 'pqr'], ['abc', 'def', 'ghi', 'jkl', 'mno', 'pqr'])
    T((99, 88), (99, 88))
    T((1, (2, 5), [8, 1], [66]), (1, 2, 5, 8, 1, 66))

testFlatten()
