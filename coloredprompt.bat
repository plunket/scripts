@echo off

::set TITLE_COLOR=yes

rem -- specify this script in HKEY_CURRENT_USER\Software\Microsoft\Command Processor\AutoRun
rem -- or make the cmd.exe shortcut do "/k this-script"
for /f %%c in ('pyw %~dp0coloredprompt.py') do set COLOR=%%c

set "PROMPT=[$T$H$H$H$H$H$H] $P$_$G "
color %COLOR%
if "%TITLE_COLOR%"=="yes" ( title %COLOR% ) else ( echo Color: %COLOR% )
