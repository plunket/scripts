#!/usr/bin/env python3

import base64
import sys

def decode_string(arg):
    decoded = base64.b64decode(arg)
    output = str(decoded, "utf-8")
    return output

def decode_hash(arg):
    decoded = base64.b64decode(arg)
    output = decoded.hex()
    return output

if __name__ == "__main__":
    args = sys.argv[1:]
    if "--hash" in args:
        args.remove("--hash")
        decode = decode_hash
    elif "-h" in args:
        args.remove("-h")
        decode = decode_hash
    else:
        decode = decode_string

    if args:
        for arg in args:
            try:
                print(f"{decode(arg)}")
            except BaseException as e:
                print(f"{arg} failed: {e}")
    else:
        for line in sys.stdin:
            line = line.strip("\n")
            print(f"{decode(line)}")
