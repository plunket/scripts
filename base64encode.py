#!/usr/bin/env python3

import base64
import sys

def encode_string(arg):
    encoded = base64.b64encode(arg.encode("utf-8"))
    output = str(encoded, "utf-8")
    return output

def encode_hash(arg):
    encoded = base64.b64encode(bytes.fromhex(arg))
    output = str(encoded, "utf-8")
    return output

if __name__ == "__main__":
    args = sys.argv[1:]
    if "--hash" in args:
        args.remove("--hash")
        encode = encode_hash
    elif "-h" in args:
        args.remove("-h")
        encode = encode_hash
    else:
        encode = encode_string

    if args:
        for arg in args:
            print(f"{encode(arg)}")
    else:
        for line in sys.stdin:
            line = line.strip("\n")
            print(f"{encode(line)}")
