#!/usr/bin/env python3

import glob
import sys

try:
    import yaml
except ModuleNotFoundError:
    print("May need to pip install PyYaml.")
    sys.exit(1)


def Convert(inFile):
    j = yaml.safe_load_all(inFile.read()) # yaml can load json... and things akin to json. :)
    y = yaml.safe_dump_all(j)#, indent=4)
    return y


if __name__ == '__main__':
    args = sys.argv[1:]
    if not args:
        args = ["-"]
    for arg in args:
        if arg == '-':
            y = Convert(sys.stdin)
            sys.stdout.write(y)
        else:
            for filename in glob.iglob(arg):
                newFilename = f"{filename}.yaml"
                print(f"{filename} -> {newFilename}")
                with open(filename, 'rt') as inFile:
                    y = Convert(inFile)
                with open(newFilename, 'wt') as outFile:
                    outFile.write(y)
