@echo off
setlocal

::echo rg %*
rg -c %*
if ERRORLEVEL 1 ( echo No matches. & exit /b %ERRORLEVEL% )

::rem -- default the qf title to the command line.
::set TITLE=rg %*
set TITLE=rg
::
::rem -- in Vim single-quoted literal-strings, a doubled single quote is interpreted as one
::set TITLE=%TITLE:'=''%
::
::rem -- when processing the command line, escape the double quotes so as to not end quoted command early.
::set TITLE=%TITLE:"=\"%
::
::set TITLE=%TITLE:|=\|%

rg --vimgrep %* | gvim --servername VRG +"call setqflist([], 'r', {'lines':getline(1,'$'),'efm':'%%f:%%l:%%c:%%m','title':'%TITLE%'})" +copen +crewind! -
