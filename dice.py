import itertools
import sys


def dice_coefficient_wikipedia(a: str, b: str) -> float:
    """From the Python implementation of the Dice-Sørensen coefficient
       algorithm for string matchiness.

    - https://en.wikipedia.org/wiki/Dice-S%C3%B8rensen_coefficient
    - https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Dice%27s_coefficient#Python
    """

    if not len(a) or not len(b): return 0.0

    # quick case for true duplicates
    if a == b: return 1.0

    # if a != b, and a or b are single chars, then they can't possibly match
    if len(a) == 1 or len(b) == 1: return 0.0

    # use python list comprehension, preferred over list.append()
    a_bigram_list = [a[i:i+2] for i in range(len(a)-1)]
    b_bigram_list = [b[i:i+2] for i in range(len(b)-1)]

    a_bigram_list.sort()
    b_bigram_list.sort()

    # assignments to save function calls
    lena = len(a_bigram_list)
    lenb = len(b_bigram_list)

    # initialize match counters
    matches = i = j = 0
    while (i < lena and j < lenb):
        if a_bigram_list[i] == b_bigram_list[j]:
            matches += 2
            i += 1
            j += 1
        elif a_bigram_list[i] < b_bigram_list[j]:
            i += 1
        else:
            j += 1

    score = float(matches) / float(lena + lenb)
    return score


def dice_coefficient_refined(a: str, b: str) -> float:
    """Modified to remove whitespace and to explicitly mark word boundaries by pairing the boundary
       letter with \0.
    """

    if not len(a) or not len(b): return 0.0

    # quick case for true duplicates
    #if a == b: return 1.0

    # if a != b, and a or b are single chars, then they can't possibly match
    #if len(a) == 1 or len(b) == 1: return 0.0

    # split into words and surround the words with boundary markers.
    def gen_pair_list(v: str) -> list[str]:
        words = v.strip().split()
        pair_gens = (itertools.pairwise(f'\0{w}\0') for w in words)
        pairs = [p for p in itertools.chain.from_iterable(pair_gens)]
        return pairs

    a_bigram_list = gen_pair_list(a)
    b_bigram_list = gen_pair_list(b)

    a_bigram_list.sort()
    b_bigram_list.sort()

    # assignments to save function calls
    lena = len(a_bigram_list)
    lenb = len(b_bigram_list)

    # initialize match counters
    matches = i = j = 0
    while (i < lena and j < lenb):
        if a_bigram_list[i] == b_bigram_list[j]:
            matches += 2
            i += 1
            j += 1
        elif a_bigram_list[i] < b_bigram_list[j]:
            i += 1
        else:
            j += 1

    score = float(matches) / float(lena + lenb)
    return score


if __name__ == "__main__":
    for args in itertools.batched(sys.argv[1:], n=2):
        print(f"{args} was: {dice_coefficient_wikipedia(*args)}, is: {dice_coefficient_refined(*args)}")
