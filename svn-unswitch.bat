@echo off
setlocal

if not "%~1"=="" (
    cd /d "%~1"
    if ERRORLEVEL 1 exit /b 1
)

svn info .. >NUL 2>&1
if ERRORLEVEL 1 (
    echo Parent directory of %CD% is not a valid SVN working directory.
    exit /b 1
)

echo Unswitching %CD%
for /f "tokens=*" %%u in ('svn info --show-item url ..') do set "PARENT_URL=%%u"
call :SetDirName "%CD%"
svn switch "%PARENT_URL%/%DIRNAME%"
echo.
echo %CD% reset to %PARENT_URL%/%DIRNAME%
goto :eof

:SetDirName
    set "DIRNAME=%~nx1"
    goto :eof
