@echo off
setlocal

set CTAGS_EXE=ctags.exe
::set CTAGS_EXCLUSIONS=--exclude=ThirdParty --exclude=Qt
set CTAGS_EXCLUSIONS=--exclude=.venv --exclude=.mypy_cache --exclude=node_modules
::set CTAGS_LANGUAGES=--languages=-JavaScript,HTML
::set CTAGS_MAPS=--map-TypeScript=+.tsx

if "%~1"=="" (
    call :MakeTags .
)

:again
if not "%~1"=="" (
    call :MakeTags %1
    shift
    goto :again
)

echo Done.
goto :eof

:NotifyDirectoryName
    echo Generating tags in %~nx1.
    goto :eof

:MakeTags
    pushd "%~1"
    call :NotifyDirectoryName %CD%.

    if exist tags.tmp del tags.tmp
    %CTAGS_EXE% -R --extras=+q --tag-relative=no %CTAGS_LANGUAGES% %CTAGS_EXCLUSIONS% %CTAGS_MAPS% -f tags.tmp .

    if exist tags del tags
    rename tags.tmp tags
    popd
    goto :eof

