#!/usr/bin/env python3

import json
import csv
import sys
import os

def csv_to_json(csv_file, json_file):
    """Convert a CSV file to JSON format."""
    try:
        # Read CSV file
        data = []
        with open(csv_file, 'r', newline='') as f:
            reader = csv.DictReader(f)
            for row in reader:
                data.append(row)

        if not data:
            print("Error: Empty CSV file")
            return False

        # Write to JSON file
        with open(json_file, 'w') as f:
            json.dump(data, f, indent=2)

        return True

    except csv.Error as e:
        print(f"Error: Invalid CSV format in {csv_file}")
        return False
    except IOError as e:
        print(f"Error: {e}")
        return False

def main():
    if len(sys.argv) != 2 and len(sys.argv) != 3:
        print("Usage: python csv2json.py input.csv [output.json]")
        sys.exit(1)

    csv_file = sys.argv[1]
    if len(sys.argv) == 2:
        # Change extension from .csv to .json
        json_file = os.path.splitext(csv_file)[0] + '.json'
    else:
        json_file = sys.argv[2]

    if not os.path.exists(csv_file):
        print(f"Error: Input file {csv_file} does not exist")
        sys.exit(1)

    if csv_to_json(csv_file, json_file):
        print(f"Successfully converted {csv_file} to {json_file}")
    else:
        sys.exit(1)

if __name__ == "__main__":
    main()
