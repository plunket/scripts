import itertools
from math import isqrt
import sys
import time
from typing import Iterator


def get_primes(max_val: int) -> Iterator[int]:
    lower_primes = []
    if max_val >= 2:
        yield 2
    split = isqrt(max_val) + 1
    for cur_val in range(3, split, 2):
        candidate_primes = itertools.takewhile(lambda p: (p*p) <= cur_val, lower_primes)
        if not any(((cur_val // p) * p == cur_val) for p in candidate_primes):
            lower_primes.append(cur_val)
            yield cur_val
    split |= 1
    for cur_val in range(split, max_val+1, 2):
        candidate_primes = itertools.takewhile(lambda p: (p*p) <= cur_val, lower_primes)
        if not any(((cur_val // p) * p == cur_val) for p in candidate_primes):
            yield cur_val


def get_prime_factors(val: int) -> Iterator[int]:
    for p in get_primes(isqrt(val)):
        while val % p == 0:
            yield p
            val //= p
            if val == 1:
                return
    if val > 1:
        yield val


def main():
    max_val = 100 if len(sys.argv) == 1 else int(sys.argv[1])
    start_time = time.time()
    primes, message = get_primes(max_val), "primes"
    #primes, message = get_prime_factors(max_val), "factors"

    if True:
        line_len = 0
        count = 0
        sep = ''
        try:
            for v in primes:
                count += 1
                line_len += 1
                print(f"{sep}{v}", end="")
                if line_len < 8:
                    sep = ", "
                else:
                    sep = ",\n"
                    line_len = 0
        except KeyboardInterrupt:
            print("\nInterrupted.", end="")
        total_time = time.time() - start_time
        print(f"\n\n{count} {message} less than or equal to {v} found in {total_time:.03f} seconds.")
    else:
        try:
            count = 0
            for v in primes:
                count += 1
        except KeyboardInterrupt:
            print("\nInterrupted.")
        total_time = time.time() - start_time
        print(f"In {total_time:.03f} seconds we got {count} primes up to {v}.")


if __name__ == "__main__":
    main()


# Up to 1000:
#   2,    3,    5,    7,   11,   13,   17,   19,
#  23,   29,   31,   37,   41,   43,   47,   53,
#  59,   61,   67,   71,   73,   79,   83,   89,
#  97,  101,  103,  107,  109,  113,  127,  131,
# 137,  139,  149,  151,  157,  163,  167,  173,
# 179,  181,  191,  193,  197,  199,  211,  223,
# 227,  229,  233,  239,  241,  251,  257,  263,
# 269,  271,  277,  281,  283,  293,  307,  311,
# 313,  317,  331,  337,  347,  349,  353,  359,
# 367,  373,  379,  383,  389,  397,  401,  409,
# 419,  421,  431,  433,  439,  443,  449,  457,
# 461,  463,  467,  479,  487,  491,  499,  503,
# 509,  521,  523,  541,  547,  557,  563,  569,
# 571,  577,  587,  593,  599,  601,  607,  613,
# 617,  619,  631,  641,  643,  647,  653,  659,
# 661,  673,  677,  683,  691,  701,  709,  719,
# 727,  733,  739,  743,  751,  757,  761,  769,
# 773,  787,  797,  809,  811,  821,  823,  827,
# 829,  839,  853,  857,  859,  863,  877,  881,
# 883,  887,  907,  911,  919,  929,  937,  941,
# 947,  953,  967,  971,  977,  983,  991,  997,
