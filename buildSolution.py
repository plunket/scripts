from __future__ import print_function

import os
import re
import subprocess
import sys

import win32api

from ElapsedTimer import ElapsedTimer

# The MSBuild command line reference is at https://msdn.microsoft.com/en-us/library/ms164311.aspx

# As of version 15 (Visual Studio 2017), there's a 'vswhere' app that can help to locate items. We
# can't use that yet.

# The earliest supported MSBuild version came with 2015, aka version 14.0.
# Earlier versions may work but the path will need to be modified.
MSBUILD_FORMAT = r"C:\Program Files (x86)\MSBuild\%s\Bin\MSBuild.exe"
MSBUILD_VERSION = None # None means to figure it out from the solution name
MSBUILD_VERSION_PATTERN = r"^20[1-2]\d$"
MSBUILD_VERSIONS = {
        '2015': '14.0',
        }

OUTPUT_FORMAT = "_build-{configuration}.{platform}.txt"

PLATFORMS = []
CONFIGURATIONS = []
FILES = []
TARGET = 'Build'

def ProcessCommandLine(argv):
    global MSBUILD_VERSION
    global PLATFORMS
    global CONFIGURATIONS
    global FILES
    global TARGET

    for a in argv:
        la = a.lower()

        if la == 'rebuild':
            TARGET = 'Rebuild'
        elif a == '32':
            PLATFORMS.append('Win32')
        elif a == '64':
            PLATFORMS.append('x64')
        elif la == 'debug':
            CONFIGURATIONS.append('Debug')
        elif la == 'release':
            CONFIGURATIONS.append('Release')
        elif os.path.exists(a):
            FILES.append(a)
        elif os.path.exists(a + '.sln'):
            FILES.append(a + '.sln')
        else:
            matches = re.match(a, MSBUILD_VERSION_PATTERN)
            if matches:
                MSBUILD_VERSION = matches.group(0)
            else:
                print("Couldn't figure out what '{}' means.".format(a))
                sys.exit(1)

    if not PLATFORMS:
        PLATFORMS = [ 'x64' ]

    if not CONFIGURATIONS:
        CONFIGURATIONS = [ 'Debug', 'Release' ]

    if not FILES:
        import glob
        FILES = glob.glob('*.sln')

def Build(solution, configuration, platform):
    msBuildVersion = MSBUILD_VERSION
    if not msBuildVersion:
        # figure out the version based on the existence or not of _2015 in the solution name
        if os.path.splitext(solution)[0].endswith('_2015'):
            msBuildVersion = '2015'
        else:
            msBuildVersion = '2008'
    msBuild = MSBUILD_FORMAT % MSBUILD_VERSIONS[msBuildVersion]

    solutionPath, solutionFile = os.path.split(solution)

    outputFile = os.path.join(solutionPath, OUTPUT_FORMAT.format(**locals()))
    if os.path.exists(outputFile):
        os.unlink(outputFile)

    print("Building {solutionFile} {configuration}|{platform} with MSBuild {msBuildVersion}."
            .format(**locals()))
    win32api.SetConsoleTitle("{solutionFile} - {configuration}|{platform}".format(**locals()))
    et = ElapsedTimer('\t\t\t\t\t\t...')

    args = [
            msBuild,
            solution,
            "/target:"+TARGET,
            "/property:Configuration=" + configuration,
            "/property:Platform=" + platform,
            "/maxcpucount",
            "/fileLogger",
            "/fileloggerparameters:LogFile=" + outputFile,
            "/verbosity:minimal",
        ]
    rv = subprocess.call(args)

    if rv != 0:
        print("Build of {solutionFile} failed for {configuration}|{platform}".format(**locals()))
    return rv

def BuildSolution(solution):
    failures = []
    for p in PLATFORMS:
        for c in CONFIGURATIONS:
            rv = Build(solution, c, p)
            if (rv != 0):
                failures.append("%s|%s" % (c, p))
    return failures

def main(argv):
    failures = {}
    ProcessCommandLine(argv[1:])

    numFailures = 0

    if FILES:
        et = ElapsedTimer("\nBuilding solutions")
        for f in FILES:
            slnFailures = BuildSolution(f)
            if slnFailures:
                failures[f] = slnFailures
    else:
        print("No solutions to build.")
        numFailures = 1

    numRun = len(FILES) * len(PLATFORMS) * len(CONFIGURATIONS)

    if failures:
        print("\n\nFailures:")
        for s, f in failures.iteritems():
            numFailures += len(f)
            print("  {}: {}".format(s, ', '.join(f)))
        print("{} total failures in {} builds.".format(numFailures, numRun))
    else:
        print("{} successful builds.".format(numRun))

    sys.exit(numFailures)

if __name__ == '__main__':
    main(sys.argv)
