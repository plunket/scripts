#!/usr/bin/env python3

"""
Convert an arbitrary data file into a "C string" and an associated length.

The data will be named k_<TranslatedName> and k_<TranslatedName>_length.
<TranslatedName> is the filename with dots converted to underscores.

    python ConvertToCString.py inputfile.dat inputfile.c

The above line will yield two data members; k_inputfile_dat will be the file
contents as a const char array and k_inputfile_dat_length will be the length of
the file.

The character array will be zero terminated for safety's sake (as it is often
used for text files and prevents many circumstances of user error) but the
given length will be the actual length of the input file.
"""

import os
import re
import string
import sys

# Visual Studio supports strings up to about 16k IIRC. Other compilers may have different limits.
MAX_STRING_LENGTH = 15000

CHARACTER_TRANSLATIONS = {}
AS_STRING = 1
AS_CHARS = 2
AS_NUMBERS = 3
AS_NUMBERS_AND_CHARS = 4

def GenerateTranslations(style, asHex=False):
    global CHARACTER_TRANSLATIONS

    if (style == AS_STRING):
        # default each character to the \x00 through \xff
        for i in range(256):
            CHARACTER_TRANSLATIONS[chr(i)] = '\\x%02x' % i

        # but translate the letters and numbers and punctuation to themselves
        for c in string.ascii_letters + string.digits + string.punctuation:
            CHARACTER_TRANSLATIONS[c] = c

        # and then handle these characters specially.
        CHARACTER_TRANSLATIONS[' '] = ' '
        CHARACTER_TRANSLATIONS['"'] = '\\"'
        CHARACTER_TRANSLATIONS['\\'] = '\\\\'
        CHARACTER_TRANSLATIONS['\n'] = '\\n'
        CHARACTER_TRANSLATIONS['\r'] = '\\r'
        CHARACTER_TRANSLATIONS['\t'] = '\\t'
        CHARACTER_TRANSLATIONS['\0'] = '\\0'
    else:
        # default each character to the \x00 through \xff
        if style == AS_CHARS:
            fmt = "'\\x%02x'"
        elif asHex:
            fmt = '0x%02x'
        elif style == AS_NUMBERS:
            fmt = '%3d'
        else: # NUMBERS_AND_CHARS !asHex
            fmt = ' %3d'

        for i in range(256):
            CHARACTER_TRANSLATIONS[chr(i)] = fmt % i

        if style == AS_NUMBERS_AND_CHARS:
            # but translate the letters and numbers and punctuation to themselves
            for c in string.ascii_letters + string.digits + string.punctuation:
                CHARACTER_TRANSLATIONS[c] = " '%s'" % c

            # and then handle these characters specially.
            CHARACTER_TRANSLATIONS[' '] = " ' '"
            CHARACTER_TRANSLATIONS['"'] = " '\"'"
            CHARACTER_TRANSLATIONS["'"] = "'\\''"
            CHARACTER_TRANSLATIONS['\\'] = "'\\\\'"
            CHARACTER_TRANSLATIONS['\n'] = "'\\n'"
            CHARACTER_TRANSLATIONS['\r'] = "'\\r'"
            CHARACTER_TRANSLATIONS['\t'] = "'\\t'"
            CHARACTER_TRANSLATIONS['\0'] = "'\\0'"

def ConvertDataName(name):
    name = os.path.split(name)[1]
    newName = [ 'k_' ]
    for c in name:
        if c in string.ascii_letters or c in string.digits:
            newName.append(c)
        else:
            newName.append('_')
    return ''.join(newName)

def FormatAsString(data):
    lines = []
    currentLine = ''
    startOfAscii = -1

    for c in data:
        if (c not in '\r\n'):
            if (c == '\t') or ((ord(c) >= 32) and (ord(c) < 127)):
                if startOfAscii == -1:
                    startOfAscii = len(currentLine)
            else:
                startOfAscii = -1

        currentLine += CHARACTER_TRANSLATIONS[c]
        if ((startOfAscii != 0) and (len(currentLine) > 90)) or (len(currentLine) > 140):
            if startOfAscii <= 0:
                lines.append(currentLine)
                currentLine = ''
                startOfAscii = -1
            else:
                lines.append(currentLine[:startOfAscii])
                currentLine = currentLine[startOfAscii:]
                startOfAscii = 0

        if (startOfAscii == 0) and (c == '\n'):
            lines.append(currentLine)
            currentLine = ''

    if currentLine:
        lines.append(currentLine)

    lines = '"\n\t"'.join(lines)

    # substitute pairs of question marks to avoid errant trigraph interpretation by the compiler.
    # http://en.wikipedia.org/wiki/Digraphs_and_trigraphs
    output = re.sub(r"[?]{2}([=/'()!<>-])", r'?\\?\1', lines)
    output = '\t"%s"' % output
    return output

def FormatAsData(data):
    output = [ '{' ]

    # put a NULL at the end in case the string is used as a string; it doesn't
    # count into the length that we printed before though.
    data = [CHARACTER_TRANSLATIONS[c] for c in data]
    data.append(CHARACTER_TRANSLATIONS['\0'])
    dataLen = len(data)
    current = 0
    while current < dataLen:
        if (current % 1024) == 0:
            output.append("\t/* byte %d */" % current)
        output.append("\t%s," % ', '.join(data[current:current+16]))
        current += 16

    output.append('}')
    output = "\n".join(output)
    return output

def Main(inputFile, outputFile, asBinary, asHex, forceString):
    with open(inputFile, 'rb') as f:
        data = f.read()

    if sys.version_info.major > 2:
        data = [chr(c) for c in data]

    dataName = ConvertDataName(inputFile)
    dataLen = len(data)

    if asBinary:
        GenerateTranslations(AS_NUMBERS, asHex=asHex)
        output = FormatAsData(data)
    elif forceString or (dataLen < MAX_STRING_LENGTH):
        GenerateTranslations(AS_STRING)
        output = FormatAsString(data)
    else:
        GenerateTranslations(AS_NUMBERS_AND_CHARS, asHex=asHex)
        output = FormatAsData(data)

    with open(outputFile, 'wt') as f:
        header = """\
/* This file was generated by a script and probably shouldn't be modified by hand. */

const unsigned int %(dataName)s_length = %(dataLen)d;
const char %(dataName)s[] =
""" % locals()
        footer = ';\n'

        f.write(header)
        f.write(output)
        f.write(footer)

if __name__ == '__main__':
    asBinary = "-b" in sys.argv
    if asBinary:
        sys.argv.remove("-b")

    asHex = "-h" in sys.argv
    if asHex:
        asBinary = True
        sys.argv.remove("-h")

    forceString = "-x" in sys.argv
    if forceString:
        sys.argv.remove("-x")

    if len(sys.argv) == 1:
        print("Need to give a filename.")
        sys.exit(1)
    else:
        infile = sys.argv[1]
        if len(sys.argv) > 2:
            outfile = sys.argv[2]
        else:
            outfile = infile + '.c'
        Main(infile, outfile, asBinary, asHex, forceString)
