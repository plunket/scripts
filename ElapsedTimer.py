from __future__ import print_function

import time

class ElapsedTimer:
    def __init__(self, taskName=None):
        if taskName:
            self.taskName = taskName
        else:
            import inspect
            stack = inspect.stack()
            prevSF = stack[1]
            #prevSF = stack[2]
            try:
                self.taskName = prevSF[3] + '()'
                #self.taskName = prevSF[4][prevSF[5]].strip()
            finally:
                del prevSF
                del stack
        self.startTime = time.time()

    def __del__(self):
        seconds = time.time() - self.startTime
        if seconds > 2.5 * 60 * 60:
            et = seconds / (60 * 60)
            quantity = 'hours'
        elif seconds > 2.5 * 60:
            et = seconds / 60
            quantity = 'minutes'
        elif seconds > 1.5:
            et = seconds
            quantity = 'seconds'
        else:
            et = seconds * 1000
            quantity = 'milliseconds'

        timeFmt = '%.0f'
        if et < 10:
            timeFmt = '%.1f'

        print("{} completed in {} {}.".format(self.taskName, timeFmt % et, quantity))

if __name__ == '__main__':
    import sys
    if (len(sys.argv) > 1) and not (sys.argv[1].startswith('/') or sys.argv[1].startswith('-')):
        import subprocess
        et = ElapsedTimer(sys.argv[1])
        subprocess.call(sys.argv[1:])
    else:
        print("""\
ElapsedTimer is intended as a module to time things in Python but can be used
standalone as well on the command line:

    ElapsedTimer.py program arg1 arg2 arg3

The above will execute 'program' with three arguments in a subshell and report
the elapsed time when it is finished.""")

