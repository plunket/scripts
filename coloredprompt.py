import sys
import random

#    0 = Black       8 = Gray
#    1 = Blue        9 = Light Blue
#    2 = Green       A = Light Green
#    3 = Aqua        B = Light Aqua
#    4 = Red         C = Light Red
#    5 = Purple      D = Light Purple
#    6 = Yellow      E = Light Yellow
#    7 = White       F = Bright White

common_divisor = 5

#    0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F
color_table = [
    -1, -1, 20, 30, -1, -1, 60, 50, 10, 25, 30, 60, 10, -1, 70, 40,     # 0
    -1, -1, 10, 10, -1, -1, 30, 40, -1, -1, 60, 60, -1, 10, 60, 50,     # 1
    10, -1, -1, -1, -1, -1, -1, 30, -1, -1, -1, -1, -1, -1, 50, 30,     # 2
    20, 30, -1, -1, -1, -1, -1, 20, -1, -1, -1, 70, -1, -1, 60, 60,     # 3
    -1, -1, -1, -1, -1, -1, -1, 15, -1, -1, -1, 10, -1, -1, 40, 30,     # 4
    -1, -1, -1, -1, -1, -1, -1, 20, -1, -1, -1, 50, -1, -1, 50, 40,     # 5
    30, 30, -1, -1, 10, -1, -1, -1, -1, 15, -1, -1, -1, -1, 20, 30,     # 6
    40, 50, 20, 15, 20, 40, -1, -1, -1, 30, -1, -1, 10, 20, -1, -1,     # 7
    15, 10, -1, -1, -1, -1, 10, 20, -1, -1, 10, 10, -1, -1, 70, 60,     # 8
    -1, -1, -1, -1, -1, -1, 10, 30, -1, -1, -1, 40, -1, -1, 70, 60,     # 9
    10, 30, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,     # A
    50, 50, -1, 10, 20, 30, -1, -1, -1, 50, -1, -1, -1, -1, -1, -1,     # B
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 20, 10,     # C
    -1, -1, -1, -1, -1, -1, -1,  5, -1, -1, -1, -1, -1, -1, 20, 10,     # D
    30, 30, 10, 10, 40, 25, 10, -1, 10, 40, -1, -1, 10, -1, -1, -1,     # E
    40, 60, 40, 30, 60, 50, 10, -1, -1, 70, -1, -1, 20, 40, -1, -1      # F
    ]

def DefineColors():
    import msvcrt
    import os
    for c in range(256):
        if color_table[c] != -1:
            os.system('cmd /c color %x' % c)
            answer = 'mmm'
            while answer not in 'gbyn\x030123456789\r ':
                print('Color %02X currently weighted %d...' % (c, color_table[c]))
                answer = msvcrt.getch()
            if answer == '\x03':
                break
            elif answer in ' \r':
                pass
            elif answer in '0123456789':
                color_table[c] = int(answer) * 10
            elif answer in 'gy':
                color_table[c] += 1
            elif color_table[c] > 0:
                color_table[c] -= 1

    os.system('cmd /c color 07')

    strs = ['%2d' % c for c in color_table]
    output = []
    startindex = 0
    while startindex < len(strs):
        output.append(', '.join(strs[startindex:startindex+16]))
        startindex += 16
    print('    ' + ',\n    '.join(output))

def ReturnColor():
    available = []
    for c in range(len(color_table)):
        if c > 0:
            available.extend([c] * (color_table[c]//common_divisor))

    print('%02X' % random.choice(available))

if __name__ == '__main__':
    if 'define' in sys.argv:
        DefineColors()
    else:
        ReturnColor()
