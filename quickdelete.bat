@echo off
setlocal

set "GONE_DIR=%~1"
set ERROR=0
if "%GONE_DIR%"=="" ( echo Must specify directory to delete. & pause & exit /b 1 )

if not exist "%GONE_DIR%" ( echo %~n1 can't be deleted because it doesn't exist. & pause & exit /b 2 )

set /p OK=Are you sure you want to delete %~nx1? ^(y/n^) ^(%~1^)
if /i not "%OK:~0,1%"=="y" ( echo Ok, aborting. & pause & exit /b 0 )

rem -- 'move' won't work on hidden directories
attrib -h %GONE_DIR%

move %GONE_DIR% %GONE_DIR%.quickdelete >nul || ( echo Couldn't rename directory. & pause & exit /b 3 )
echo.
echo Directory successfully renamed; delete starting.
echo.

rd /s /q %GONE_DIR%.quickdelete || ( echo Couldn't delete temporary directory but it's moved. & pause & exit /b 4 )

echo Done.
echo.
timeout /t 10

