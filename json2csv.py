#!/usr/bin/env python3

import json
import csv
import sys
import os

def json_to_csv(json_file, csv_file):
    """Convert a JSON file to CSV format."""
    try:
        # Read JSON file
        with open(json_file, 'r') as f:
            data = json.load(f)

        # Handle both single objects and lists of objects
        if not isinstance(data, list):
            data = [data]

        # Validate that all values are simple (not lists or objects)
        for item in data:
            for value in item.values():
                if isinstance(value, (list, dict)):
                    print("Error: Complex values (lists or objects) are not supported")
                    return False

        if not data:
            print("Error: Empty JSON file")
            return False

        # Get headers from first object and validate all entries have same keys
        headers = list(data[0].keys())
        for item in data[1:]:
            if set(item.keys()) != set(headers):
                print("Error: Not all JSON objects have the same keys")
                return False

        # Write to CSV file
        with open(csv_file, 'w', newline='') as f:
            writer = csv.DictWriter(f, fieldnames=headers)
            writer.writeheader()
            writer.writerows(data)

        return True

    except json.JSONDecodeError:
        print(f"Error: Invalid JSON format in {json_file}")
        return False
    except IOError as e:
        print(f"Error: {e}")
        return False

def main():
    if len(sys.argv) != 2 and len(sys.argv) != 3:
        print("Usage: python json2csv.py input.json [output.csv]")
        sys.exit(1)

    json_file = sys.argv[1]
    if len(sys.argv) == 2:
        # Change extension from .json to .csv
        csv_file = os.path.splitext(json_file)[0] + '.csv'
    else:
        csv_file = sys.argv[2]

    if not os.path.exists(json_file):
        print(f"Error: Input file {json_file} does not exist")
        sys.exit(1)

    if json_to_csv(json_file, csv_file):
        print(f"Successfully converted {json_file} to {csv_file}")
    else:
        sys.exit(1)

if __name__ == "__main__":
    main()
