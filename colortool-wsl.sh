#!/usr/bin/env bash

# Source this in .profile after switching to its directory.
#   pushd "/mnt/d/path/to/ColorTool"
#   . coloredprompt.sh
#   popd

#cd "$(dirname "$0")"

scheme=$( ls schemes/*.itermcolors | shuf -n 1 )

./ColorTool.exe --quiet --xterm "${scheme}"

SCHEMENAME=$( basename -s .itermcolors "${scheme}" )
export SCHEMENAME
